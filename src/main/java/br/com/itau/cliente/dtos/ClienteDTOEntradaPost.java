package br.com.itau.cliente.dtos;

import br.com.itau.cliente.models.Cliente;

public class ClienteDTOEntradaPost {

    private String name;

    public ClienteDTOEntradaPost() { }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

}
