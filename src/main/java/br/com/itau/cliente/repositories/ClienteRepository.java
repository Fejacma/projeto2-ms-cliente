package br.com.itau.cliente.repositories;

import br.com.itau.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> { }
